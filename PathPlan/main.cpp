#include <defines.h>
#include <includes.h>
#include <map>
#include <tinyxml.h>
#include <structs.h>
#include <inputfunc.h>
#include <logger.h>
#include <map.h>
#include <searchfunc.h>
#include <search.h>
#include <malloc.h>

Logger DoSearch(const char *searchtype, const char *filename, const char *newfilename,
                const std::vector<std::vector<int>>& grid, double cellsize,
                int startx, int starty, int finishx, int finishy) {
    if (strcmp(searchtype, ASTARNAME) == 0) {
       free((void *)searchtype);
        AStar TheSearch;
        TheSearch.ReadfromXml(filename);
        return TheSearch.DoAstar(grid, newfilename, cellsize, startx, starty, finishx, finishy);
    }
    if (strcmp(searchtype, JPSNAME) == 0) {
        free((void *)searchtype);
        JPSearch TheSearch;
        TheSearch.ReadfromXml(filename);
        return TheSearch.DoAstar(grid, newfilename, cellsize, startx, starty, finishx, finishy);
    }
    if (strcmp(searchtype, THETANAME) == 0) {
        free((void *)searchtype);
        ThetaStar TheSearch;
        TheSearch.ReadfromXml(filename);
        return TheSearch.DoAstar(grid, newfilename, cellsize, startx, starty, finishx, finishy);
    }
    free((void *)searchtype);
    Search TheSearch;
    TheSearch.ReadfromXml(filename);
    return TheSearch.DoAstar(grid, newfilename, cellsize, startx, starty, finishx, finishy);
}

const char *CreateLogName(const char *name) {
    char *ptr;
    ptr = (char *)name;
    int last = -1;
    int counter = 0;
    while (*ptr != 0) {
        if (*ptr == '.') last = counter;
        ++counter;
        ++ptr;
    }
    if (last == -1) return name;
    char *res = (char *)malloc(strlen(name) + 4);
    *res = '\0';
    strcat(res, name);
    *(res + last) = '\0';
    strcat(res, "_log");
    strcat(res, (char *)name + last);
    const char *result = res;
    return result;
}

const char *GetSearch(const char *filename) {
    const char *searchtype;
    TiXmlDocument doc = TiXmlDocument(filename);
    bool loadOkay = doc.LoadFile();
    if (loadOkay) {
        TiXmlElement *root = doc.FirstChildElement("root");
        if (root) {
            TiXmlElement *algorithm = root->FirstChildElement(ALGORITHMNAME);
            if (algorithm) {
                if (GetStr(algorithm, SEARCHTYPENAME, searchtype)) {
                    searchtype = ASTARNAME;
                    PrintWarning(SEARCHTYPENAME);
                }
            } else {
                PrintError(ALGORITHMNAME);
                return NULL;
            }
        } else {
            PrintError("root");
            return NULL;
        }
    } else {
        std::cout << "Loading file error\n";
        return NULL;
    }
    return searchtype;
}

int CreateXml(const char *oldname, const char *newname) {
    TiXmlDocument doc = TiXmlDocument(oldname);
    if(!doc.LoadFile()) return 0;
    auto *root = doc.RootElement();
    TiXmlElement *lowlevel = new TiXmlElement(LOWLEVELNAME);
    TiXmlElement *log = new TiXmlElement(LOGNAME);
    log->LinkEndChild(lowlevel);
    root->LinkEndChild(log);
    if(!doc.SaveFile(newname)) return 0;
    return 1;
}

int main(int argc, char* argv[]) {
    if (argc < 2) {
        std::cout << "Path for input file was not given. Please, add it and try again!\n";
        return 0;
    }
    const char * filename = argv[1];
    Map map;
    if (map.ReadfromXml(filename)) {
        std::cout << "Fatal error in input. Path can't be calculated!\n";
        return 0;
    }
    const char *searchtype = GetSearch(filename);
    if (searchtype == NULL) {
        std::cout << "Fatal error in input. Path can't be calculated!\n";
        return 0;
    }
    const char *newfilename = CreateLogName(filename);
    CreateXml(filename, newfilename);
    Logger log = DoSearch(searchtype, filename, newfilename, map.grid, map.cellsize,
                          map.startx, map.starty, map.finishx, map.finishy);
    log.WritetoXml(newfilename, map.grid);
}
