#include <algorithm>
#include <iostream>
#include <limits>
#include <vector>

struct heap_element {
    int key;
    int element;
};

bool operator < (const heap_element& first, const heap_element& second) {
    return (first.element < second.element);
}

class Heap {
private:
    std::vector<heap_element> heap;
    std::vector<int> place;

int first_child(int current) {
    return 2 * current + 1;
}

int second_child(int current) {
    return 2 * current + 2;
}

int parent(int current) {
    return (current - 1) / 2;
}

public:
    Heap(int amount, int value = 0) {
        heap_element element;
        element.element = value;
        for (int number = 0; number != amount; ++number) {
            element.key = number;
            heap.push_back(element);
            place.push_back(number);
        }
    };

    template <typename It>
    Heap(It begin, It end) {
        heap_element element;
        int number = 0;
        while (begin != end) {
            element.element = *begin;
            element.key = number;
            heap.push_back(element);
            place.push_back(number);
            int current = heap.size() - 1;
            while (current >= 1 && heap[current] < heap[parent(current)]) {
                std::swap(heap[current], heap[parent(current)]);
                std::swap(place[heap[current].key],
                          place[heap[parent(current)].key]);
                current = parent(current);
            }
            ++begin;
            ++number;
        }
    };

    Heap(std::initializer_list<int> values) : Heap(values.begin(), values.end()) {};

    int Shift_down(int current) {
        int another;
        if ((second_child(current) < heap.size()) &&
            (heap[second_child(current)] < heap[first_child(current)])) {
            another = second_child(current);
        } else {
            another = first_child(current);
        }
        if (heap[another] < heap[current]) {
            std::swap(heap[another], heap[current]);
            std::swap(place[heap[another].key], place[heap[current].key]);
        }
        return another;
    }

    size_t top() const {
        return heap[0].key;
    }

    void pop() {
        if (heap.size() != 0) {
            int current = 0;
            std::swap(heap[0], heap[heap.size() - 1]);
            std::swap(place[heap[0].key], place[heap[heap.size() - 1].key]);
            heap.pop_back();
            while (first_child(current) < heap.size()) {
                current = Shift_down(current);
            }
        }
    }

    void decrease_key(int key, int value) {
        int current = place[key];
        heap[current].element = value;
        while (current >= 1 && heap[current] < heap[parent(current)]) {
            std::swap(heap[current], heap[parent(current)]);
            std::swap(place[heap[current].key], place[heap[parent(current)].key]);
            current = parent(current);
        }
    }

    bool empty() const {
        return heap.empty();
    }

    int size() const {
        return heap.size();
    }
};

std::vector<int> calculate_path(std::vector<std::vextor<arrow>>& graph, int start, int finish) {
    const int INF = std::numeric_limits<int>::max() / 2;
    std::vector<double> distance (graph.size(), INF);
    std::vector<int> parent;
    distance[start] = 0;
    Heap unchecked(distance.begin(), distance.end());
    while (!unchecked.empty()) {
        int vertex = unchecked.top(),  current_distance = distance[unchecked.top()];
        unchecked.pop();
        for (int number = 0; number != graph[vertex].size(); ++number) {
            int to = graph[vertex][number].dest;
            int length = roads[vertex][number].length;
            if (distance[vertex] + length < distance[to]) {
                distance[to] = distance[vertex] + length;
                unchecked.decrease_key(to, distance[to]);
                parent[to] = vertex;
            }
        }
    }
    if (distance[finish] != INF) {
        return distance[finish];
    }
    return -1;
}
