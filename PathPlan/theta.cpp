#include <vector>
#include <map>
#include <structs.h>
#include <logger.h>
#include <math.h>
#include <searchfunc.h>
#include <string.h>
#include <search.h>

bool ThetaStar::LineofSight(const std::vector<std::vector<int>>& grid, int x1, int y1, int x2, int y2) {
    int deltaX = abs(x2 - x1);
    int deltaY = abs(y2 - y1);
    int signX = x1 < x2 ? 1 : -1;
    int signY = y1 < y2 ? 1 : -1;
    int error = deltaX - deltaY;
    int prev_x, prev_y;
    bool if_first = true;
    while(x1 != x2 || y1 != y2) {
        if (!if_first && !IfNeighbour(grid, prev_x, prev_y, x1, y1, abs(x1 - prev_x) + abs(y1 - prev_y))) return false;
        prev_x = x1;
        prev_y = y1;
        int error2 = error * 2;
        if(error2 > -deltaY) {
            error -= deltaY;
            x1 += signX;
        }
        if(error2 < deltaX) {
            error += deltaX;
            y1 += signY;
        }
        if_first = false;
    }
    if (!IfNeighbour(grid, prev_x, prev_y, x1, y1, abs(x1 - prev_x) + abs(y1 - prev_y))) return false;
    return true;
}

void ThetaStar::AddtoPath(std::vector<Node>& path, int x1, int y1, int x2, int y2) {
    int deltaX = abs(x2 - x1);
    int deltaY = abs(y2 - y1);
    int signX = x1 < x2 ? 1 : -1;
    int signY = y1 < y2 ? 1 : -1;
    int error = deltaX - deltaY;
    int prev_x, prev_y;
    while(x1 != x2 || y1 != y2) {
        path.push_back(makeNode(x1, y1, 0, 0, NULL));
        prev_x = x1;
        prev_y = y1;
        int error2 = error * 2;
        if(error2 > -deltaY) {
            error -= deltaY;
            x1 += signX;
        }
        if(error2 < deltaX) {
            error += deltaX;
            y1 += signY;
        }
    }
}

void ThetaStar::ResetParent(const std::vector<std::vector<int>>& grid, Node& node, int startx, int starty) {
    if ((node.parent) -> x != startx || (node.parent) -> y != starty) {
        Node try_parent = *((node.parent) -> parent);
        if (LineofSight(grid, try_parent.x, try_parent.y, node.x, node.y)) {
            node = makeNode(node.x, node.y, try_parent.g_func + dist(try_parent.x, try_parent.y, node.x, node.y),
                            node.f_func - node.g_func, (node.parent) -> parent);
        }
    }
}

std::vector<Node> ThetaStar::MakeShort(const std::vector<Node>& path) {
    return path;
}

std::vector<Node> ThetaStar::MakeLong(const std::vector<Node>& path) {
    std::vector<Node> result;
    for (size_t i = 1; i != path.size(); ++i) {
        AddtoPath(result, path[i - 1].x, path[i - 1].y, path[i].x, path[i].y);
    }
    result.push_back(path[path.size() - 1]);
    return result;
}
