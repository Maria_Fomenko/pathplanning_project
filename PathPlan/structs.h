//#include <list>

struct arrow {
    int dest;
    double length;
};

struct vertex {
    int x;
    int y;
};

struct step {
    int x;
    int y;
    double length;
};

struct Node {
    int x;
    int y;
    double g_func;
    double f_func;
    Node *parent;
    //int parent_x;
    //int parent_y;
};
