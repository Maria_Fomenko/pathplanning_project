#include <tinyxml.h>

int GetInt(TiXmlElement*, const char*, int&);

int GetFlt(TiXmlElement*, const char*, double&);

int GetStr(TiXmlElement*, const char*, const char*&);

int GetBool(TiXmlElement*, const char*, bool&);

void PrintWarning(const char*);

int PrintError(const char*);

