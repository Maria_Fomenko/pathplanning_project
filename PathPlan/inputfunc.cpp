#include <inputfunc.h>
#include <iostream>

int GetInt(TiXmlElement* field, const char* name, int& value) {
    TiXmlElement *elem = field->FirstChildElement(name);
    if (elem) {
        const char* str = elem->GetText();
        char *end;
        value = strtol(str, &end, 10);
        if (value < 0 || *end) return 1;
        return 0;
    }
    return 1;
}

int GetFlt(TiXmlElement* field, const char* name, double& value) {
    TiXmlElement *elem = field->FirstChildElement(name);
    if (elem) {
        const char* str = elem->GetText();
        char *end;
        value = strtof(str, &end);
        if (value < 0 || *end) return 1;
        return 0;
    }
    return 1;
}

int GetStr(TiXmlElement* field, const char* name, const char*& value) {
    TiXmlElement *elem = field->FirstChildElement(name);
    if (elem) {
        value = (const char *)calloc(1, strlen(elem->GetText()));
        strcpy((char *)value, elem->GetText());
        return 0;
    }
    return 1;
}

int GetBool(TiXmlElement* field, const char* name, bool& value) {
    TiXmlElement *elem = field->FirstChildElement(name);
    if (elem) {
        const char* str = elem->GetText();
        if (strcmp(str, "1") == 0 || strcmp(str,"true") == 0) {
            value = true;
            return 0;
        }
        if (strcmp(str, "0") == 0 || strcmp(str, "false") == 0) {
            value = false;
            return 0;
        }
    }
    return 1;
}

void PrintWarning(const char* field) {
    std::cout << "Warning: " << field << " is not found or written incorrectly and set by default\n";
}

int PrintError(const char* field) {
    std::cout << "Error: " << field << " is not found in document or written incorrectly\n";
    return 1;
}
