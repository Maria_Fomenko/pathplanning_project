//#include <string.h>
//#include <sstream>
//#include <map.h>

template <typename T>
const char *toCString(T val) {
    std::ostringstream oss;
    oss << val;
    std::string strres = oss.str();
    return strres.c_str();
}
