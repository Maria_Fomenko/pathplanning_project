#include <defines.h>
#include <chrono>
#include <malloc.h>
#include <map>
#include <includes.h>
#include <tinyxml.h>
#include <structs.h>
#include <inputfunc.h>
#include <map.h>
#include <logger.h>
#include <searchfunc.h>
#include <search.h>

int Search::WriteList(const std::list<Node>& opened, const std::map<int, Node>& closed, const char *newfilename, int stnumber) {
    TiXmlDocument doc = TiXmlDocument(newfilename);
    if(!doc.LoadFile()) return 0;
    auto *root = doc.RootElement();
    TiXmlElement *log = root->FirstChildElement(LOGNAME);
    TiXmlElement *lowlevel = log->FirstChildElement(LOWLEVELNAME);
    TiXmlElement *step = new TiXmlElement(STEPNAME);
    step->SetAttribute("number", stnumber);
    TiXmlElement *open = new TiXmlElement(OPENNAME);
    TiXmlElement *close = new TiXmlElement(CLOSENAME);
    for (auto elem : opened) {
        TiXmlElement *node = new TiXmlElement(NODENAME);
        node->SetAttribute(XNAME, elem.x);
        node->SetAttribute(YNAME, elem.y);
        node->SetDoubleAttribute(FNAME, elem.f_func);
        node->SetDoubleAttribute(GNAME, elem.g_func);
        if (elem.g_func != 0) {
            node->SetAttribute(PARENTXNAME, (elem.parent)->x);
            node->SetAttribute(PARENTYNAME, (elem.parent)->y);
        }
        open->LinkEndChild(node);
    }
    step->LinkEndChild(open);
    for (auto elem : closed) {
        TiXmlElement *node = new TiXmlElement(NODENAME);
        node->SetAttribute(XNAME, elem.second.x);
        node->SetAttribute(YNAME, elem.second.y);
        node->SetDoubleAttribute(FNAME, elem.second.f_func);
        node->SetDoubleAttribute(GNAME, elem.second.g_func);
        if (elem.second.g_func != 0) {
            node->SetAttribute(PARENTXNAME, (elem.second.parent)->x);
            node->SetAttribute(PARENTYNAME, (elem.second.parent)->y);
        }
        close->LinkEndChild(node);
    }
    step->LinkEndChild(close);
    lowlevel->LinkEndChild(step);
    doc.SaveFile(newfilename);
    return 1;
}


double Search::dist(int x1, int y1, int x2, int y2) {
    if (strcmp(metrictype, EUCLIDNAME) == 0) return linecost * sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
    else if (strcmp(metrictype, CHEBYSHEVNAME) == 0) return linecost * std::max(abs(x2 - x1), abs(y2 - y1));
    else if (strcmp(metrictype, DIAGONALNAME) == 0) return diagonalcost * std::min(abs(x2 - x1), abs(y2 - y1)) + linecost * abs(abs(x2 - x1) - abs(y2 - y1));
    else if (strcmp(metrictype, MANHATHANNAME) == 0) return linecost * (abs(x2 - x1) + abs(y2 - y1));
    return 0;
}

bool Search::IfNeighbour(const std::vector<std::vector<int>>& grid, int x0, int y0, int x1, int y1, int sum) {
    if (sum == 0 || x1 == -1 || y1 == -1 || x1 == grid[y0].size() || y1 == grid.size() || grid[y1][x1] == 1) return false;
    bool if_first = (grid[y0][x1] == 0);
    bool if_second = (grid[y1][x0] == 0);
    return sum == 1 || (sum == 2 && allowdiagonal && ((allowsqueeze && cutcorners) || (cutcorners && (if_first || if_second)) || (if_first && if_second)));
}

bool Search::comp(const Node& first, const Node& second) {
    bool g_comp = 1;
    if (strcmp(breakingties, GMAXNAME) == 0) g_comp = first.g_func > second.g_func;
    else if (strcmp(breakingties, GMINNAME) == 0) g_comp = first.g_func < second.g_func;
    return (first.f_func < second.f_func) || ((first.f_func == second.f_func) && g_comp);
}

void Search::AddtoList(std::list<Node>& thelist, Node elem) {
    auto iter = thelist.begin();
    auto end = thelist.end();
    while (iter != end && comp(*iter, elem)) ++iter;
    thelist.insert(iter, elem);
}

Node Search::makeNode(int x, int y, double g_f, double h_f, Node *pr) {
    Node node;
    node.x = x;
    node.y = y;
    node.g_func = g_f;
    node.f_func = g_f + hweight * h_f;
    node.parent = pr;
    //node.parent_x = pr_x;
    //node.parent_y = pr_y;
    return node;
}

std::vector<Node> Search::GetNeighbours(const std::vector<std::vector<int>>& grid, Node *cur,
                                        int finx, int finy) {
    std::vector<Node> result;
    for (int i = -1; i != 2; ++i) {
        for (int j = -1; j != 2; ++j) {
            int sum = abs(i) + abs(j);
            int x = cur->x + i;
            int y = cur->y + j;
            if (IfNeighbour(grid, cur->x, cur->y, x, y, sum)) {
                if (sum == 1) {
                    result.push_back(makeNode(x, y, cur->g_func + linecost, dist(x, y, finx, finy), cur));
                } else if (sum == 2) {
                    result.push_back(makeNode(x, y, cur->g_func + diagonalcost, dist(x, y, finx, finy), cur));
                }
            }
        }
    }
    return result;
}

int Search::ReadfromXml(const char* filename) {
    TiXmlDocument doc = TiXmlDocument(filename);
    bool loadOkay = doc.LoadFile(filename);
    if (loadOkay) {
        TiXmlElement *root = doc.FirstChildElement("root");
        if (root) {
            TiXmlElement *algorithm = root->FirstChildElement(ALGORITHMNAME);
            if (algorithm) {
                if (GetStr(algorithm, METRICTYPENAME, metrictype)) {
                    metrictype = "euclid";
                    PrintWarning(METRICTYPENAME);
                }
                if (GetStr(algorithm, SEARCHTYPENAME, searchtype)) {
                    searchtype = "astar";
                    PrintWarning(SEARCHTYPENAME);
                }
                if (GetFlt(algorithm, HWEIGHTNAME, hweight)) {
                    hweight = 1;
                    PrintWarning(HWEIGHTNAME);
                }
                if (GetStr(algorithm, BREAKINGTIESNAME, breakingties)) {
                    breakingties = "g-max";
                    PrintWarning(BREAKINGTIESNAME);
                }
                if (GetFlt(algorithm, LINECOSTNAME, linecost)) {
                    linecost = 1;
                    PrintWarning(LINECOSTNAME);
                }
                if (GetFlt(algorithm, DIAGONALCOSTNAME, diagonalcost)) {
                    diagonalcost = 1.414;
                    PrintWarning(DIAGONALCOSTNAME);
                }
                if (GetBool(algorithm, ALLOWDIAGONALNAME, allowdiagonal)) {
                    allowdiagonal = 0;
                    PrintWarning(ALLOWDIAGONALNAME);
                }
                if (GetBool(algorithm, ALLOWSQUEEZENAME, allowsqueeze)) {
                    allowsqueeze = 0;
                    PrintWarning(ALLOWSQUEEZENAME);
                }
                if (GetBool(algorithm, CUTCORNERSNAME, cutcorners)) {
                    cutcorners = 0;
                    PrintWarning(CUTCORNERSNAME);
                }
            } else return PrintError(ALGORITHMNAME);
            TiXmlElement *options = root->FirstChildElement(OPTIONSNAME);
            if (options) {
                if (GetFlt(options, LOGLEVELNAME, loglevel)) {
                    loglevel = 1;
                    PrintWarning(LOGLEVELNAME);
                }
            } else return PrintError(OPTIONSNAME);
        } else return PrintError("root");
    } else {
        std::cout << "Loading file error\n";
        return 1;
    }
    return 0;
}

std::vector<Node> Search::MakeShort(const std::vector<Node>& path) {
    std::vector<Node> result;
    int prev_dx, prev_dy;
    int dx = 0;
    int dy = 0;
    for (size_t i = 0; i != path.size() - 1; ++i) {
        prev_dx = dx;
        prev_dy = dy;
        dx = path[i + 1].x - path[i].x;
        dy = path[i + 1].y - path[i].y;
        if (prev_dx != dx || prev_dy != dy) {
            result.push_back(path[i]);
        }
    }
    result.push_back(path[path.size() - 1]);
    return result;
}

std::vector<Node> Search::MakeLong(const std::vector<Node>& path) {
    return path;
}


Logger Search::DoAstar(const std::vector<std::vector<int>>& grid, const char *newfilename, double cellsize, int startx, int starty, int finishx, int finishy) {
    std::list<Node> opened;
    std::map<int, Node> closed;
    Node *current;
    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();
    int stepscounter = 1;
    opened.push_back(makeNode(startx, starty, 0, dist(startx, starty, finishx, finishy), current));
    while (!opened.empty()) {
        auto iter = opened.begin();
        if (loglevel == 2 || loglevel > 1 && iter->x == finishx && iter->y == finishy) {
            WriteList(opened, closed, newfilename, stepscounter - 1);
        }
        int key = iter->x * grid.size() + iter->y;
        closed.insert(std::pair<int, Node>(key, *iter));
        opened.erase(iter);
        auto cur = closed.find(key);
        current = &(cur->second);
        if (current->x == finishx && current->y == finishy) break;
        std::vector<Node> neighbours = GetNeighbours(grid, current, finishx, finishy);
        for (auto elem : neighbours) {
            bool if_closed = 0;
            bool if_opened = 0;
            if (closed.find(elem.x * grid.size() + elem.y) != closed.end()) if_closed = 1;
            if (!if_closed) {
                ResetParent(grid, elem, startx, starty);
                auto node = opened.begin();
                while (node != opened.end() && !if_opened) {
                    if (node->x == elem.x && node->y == elem.y) {
                        if_opened = 1;
                        if (node->g_func > elem.g_func) {
                            opened.erase(node);
                            AddtoList(opened, elem);
                        }
                    }
                    ++node;
                }
            }
            if (!if_opened && !if_closed) AddtoList(opened, elem);
        }
        ++stepscounter;
    }
    free((void *)searchtype);
    free((void *)metrictype);
    free((void *)breakingties);
    if (current->x != finishx || current->y != finishy) std::cout << "The path does not exist\n";
    else {
        std::cout << "The path is successfully calculated:\n";
        int nodescreated = opened.size() + closed.size();
        int numberofsteps = stepscounter;
        double length = current->g_func;
        double time;
        std::vector<Node> path;
        while (current->x != startx || current->y != starty) {
            path.push_back(*current);
            current = current->parent;
        }
        path.push_back(*current);
        end = std::chrono::system_clock::now();
        time = static_cast<double>(std::chrono::duration_cast<std::chrono::nanoseconds>(end - start).count()) / 1000000000;
        std::reverse(path.begin(), path.end());
        std::vector<Node> shortpath = MakeShort(path);
        std::vector<Node> longpath = MakeLong(path);
        Logger result(shortpath, longpath, nodescreated, numberofsteps, length, length * cellsize, time);
        result.write();
        return result;
    }
    Logger emptyresult;
    return emptyresult;
}
