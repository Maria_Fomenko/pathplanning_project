#include <list>
#include <structs.h>
#include <defines.h>
#include <tinyxml.h>
#include <iostream>
#include <inputfunc.h>
#include <logger.h>

int Logger::WritetoXml(const char *newfilename, std::vector<std::vector<int>>& grid) {
    TiXmlDocument doc = TiXmlDocument(newfilename);
    if(!doc.LoadFile()) return 0;
    auto *root = doc.RootElement();
    TiXmlElement *summary = new TiXmlElement(SUMMARYNAME);
    summary->SetAttribute(NODESCRTDNAME, nodescreated);
    summary->SetAttribute(NUMOFSTEPSNAME, numberofsteps);
    summary->SetDoubleAttribute(LENGTHNAME, length);
    summary->SetDoubleAttribute(SCALEDLENNAME, scaledlength);
    summary->SetDoubleAttribute(TIMENAME, time);
    size_t sizey = grid.size();
    size_t sizex = grid[0].size();
    TiXmlElement *lplevel = new TiXmlElement(LPLEVELNAME);
    for (size_t i = 0; i != longpath.size(); ++i) {
        grid[longpath[i].y][longpath[i].x] = 2;
        TiXmlElement *node = new TiXmlElement(NODENAME);
        node->SetAttribute(XNAME, longpath[i].x);
        node->SetAttribute(YNAME, longpath[i].y);
        node->SetAttribute(NUMBERNAME, i);
        lplevel->LinkEndChild(node);
    }
    TiXmlElement *hplevel = new TiXmlElement(HPLEVELNAME);
    for (size_t i = 1; i != shortpath.size(); ++i) {
        TiXmlElement *section = new TiXmlElement(SECTIONNAME);
        section->SetAttribute(NUMBERNAME, i - 1);
        section->SetAttribute(SSTARTX, shortpath[i - 1].x);
        section->SetAttribute(SSTARTY, shortpath[i - 1].y);
        section->SetAttribute(SFINISHX, shortpath[i].x);
        section->SetAttribute(SFINISHY, shortpath[i].y);
        section->SetDoubleAttribute(LENGTHNAME, shortpath[i].g_func - shortpath[i - 1].g_func);
        hplevel->LinkEndChild(section);
    }
    TiXmlElement *path = new TiXmlElement(PATHNAME);
    for (size_t y = 0; y != sizey; ++y) {
        TiXmlElement *row = new TiXmlElement(ROWNAME);
        row->SetAttribute(NUMBERNAME, y);
        std::string the_row;
        the_row.resize(sizex * 2 - 1);
        auto iter = the_row.begin();
        for (size_t x = 0; x != sizex; ++x) {
            if (grid[y][x] == 2) *iter = '*';
            else *iter = '0' + grid[y][x];
            ++iter;
            if (iter == the_row.end()) break;
            *iter = ' ';
            ++iter;
        }
        std::cout << the_row << "\n";
        TiXmlText* text = new TiXmlText(the_row.c_str());
        row->LinkEndChild(text);
        path->LinkEndChild(row);
    }
    TiXmlElement *log = root->FirstChildElement(LOGNAME);
    log->LinkEndChild(summary);
    log->LinkEndChild(path);
    log->LinkEndChild(lplevel);
    log->LinkEndChild(hplevel);
    if(!doc.SaveFile(newfilename)) return 0;
    return 1;
}

void Logger::write() {
    std::cout << nodescreated << "\n" << numberofsteps << "\n" << length << "\n" << time << "\n";
    std::cout << "longpath:\n";
    for (size_t i = 0; i != longpath.size(); ++i) {
        std::cout << longpath[i].x << " " << longpath[i].y << "\n";
    }
    std::cout << "shortpath:\n";
    for (size_t i = 0; i != shortpath.size(); ++i) {
        std::cout << shortpath[i].x << " " << shortpath[i].y << "\n";
    }
}

