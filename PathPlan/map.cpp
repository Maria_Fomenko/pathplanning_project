#include <map.h>
#include <tinyxml.h>
#include <defines.h>
#include <inputfunc.h>
#include <iostream>

int Map::ReadfromXml(const char* filename) {
    TiXmlDocument doc = TiXmlDocument(filename);
    bool loadOkay = doc.LoadFile();
    if (loadOkay) {
        std::vector<int> new_row;
        TiXmlElement *root = doc.FirstChildElement("root");
        if (root) {
            TiXmlElement *map = root->FirstChildElement(MAPNAME);
            if (map) {
                if (GetInt(map, WIDTHNAME, width)) return PrintError(WIDTHNAME);
                if (GetInt(map, HEIGHTNAME, height)) return PrintError(HEIGHTNAME);
                if (GetFlt(map, CELLSIZENAME, cellsize)) {
                    cellsize = 1;
                    PrintWarning(CELLSIZENAME);
                }
                if (GetInt(map, STARTXNAME, startx)) return PrintError(STARTXNAME);
                if (GetInt(map, STARTYNAME, starty)) return PrintError(STARTYNAME);
                if (GetInt(map, FINISHXNAME, finishx)) return PrintError(FINISHXNAME);
                if (GetInt(map, FINISHYNAME, finishy)) return PrintError(FINISHYNAME);
                TiXmlElement *fgrid = map->FirstChildElement(GRIDNAME);
                if (fgrid) {
                    TiXmlElement *row = fgrid->FirstChildElement(ROWNAME);
                    int counter = 0;
                    while (row != NULL && counter != height) {
                        grid.push_back(new_row);
                        const char* str = row->GetText();
                        int row_counter = 0;
                        while (*str != '\0' && row_counter != width) {
                            int cell;
                            if (*str != ' ') {
                                if (*str == FREENAME) cell = 0;
                                else if (*str == BLOCKEDNAME) cell = 1;
                                grid[counter].push_back(cell);
                                ++row_counter;
                            }
                            ++str;
                        }
                        if (*str != '\0') {
                            std::cout << "Warning: row number " << counter << " is too long and cut\n";
                        }
                        if (row_counter != width) {
                            std::cout << "Error: row number " << counter << " is too short\n";
                            return 1;
                        }
                        row = row->NextSiblingElement(ROWNAME);
                        ++counter;
                    }
                    if (row != NULL) {
                        std::cout << "Warning: too many rows. Only first " << height << " are taken in account\n";
                    }
                    if (counter != height) {
                        std::cout << "Error: not enough rows\n";
                        return 1;
                    }
                } else return PrintError(GRIDNAME);
            } else return PrintError(MAPNAME);
        } else return PrintError("root");
    } else {
        std::cout << "Loading file error\n";
        return 1;
    }
    if (startx >= width || finishx >= width || starty >= height || finishy >= height ||
        grid[starty][startx] == 1 || grid[finishy][finishx] == 1) {
        std::cout << "Error: incorrect coordinates of start and/or finish cell\n";
        return 1;
    }
    return 0;
}
