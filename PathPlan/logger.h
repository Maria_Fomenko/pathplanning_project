//#include <outputfunc.h>
//#include <structs.h>
#include <vector>
#include <list>

class Logger {
private:
    std::vector<Node> longpath;
    std::vector<Node> shortpath;
    int nodescreated;
    int numberofsteps;
    double length;
    double scaledlength;
    double time;
    bool is_empty;
    //std::vector<std::list<Node>> opened_list;
    //std::vector<std::list<Node>> closed_list;

public:

Logger(std::vector<Node> spath, std::vector<Node> lpath, int nodescr, int numofst, double len, double sclen, double tm) {
    longpath = lpath;
    shortpath = spath;
    nodescreated = nodescr;
    numberofsteps = numofst;
    length = len;
    scaledlength = sclen;
    time = tm;
    is_empty = 0;
}

Logger() {
    is_empty = 1;
}

int WritetoXml(const char *, std::vector<std::vector<int>>&);

void write();

};

int WriteList(const std::list<Node>&, const std::list<Node>&, const char *, int);
