class Search {
protected:
    const char* metrictype;
    const char* searchtype;
    double hweight;
    const char* breakingties;
    double linecost;
    double diagonalcost;
    bool allowdiagonal;
    bool allowsqueeze;
    bool cutcorners;
    double loglevel;

public:

int WriteList(const std::list<Node>&, const std::map<int, Node>&, const char *, int);

bool comp(const Node&, const Node&);

void AddtoList(std::list<Node>&, Node);

Node makeNode(int, int, double, double, Node *);

virtual void ResetParent(const std::vector<std::vector<int>>& grid, Node& node, int startx, int starty) {}

double dist(int, int, int, int);

bool IfNeighbour(const std::vector<std::vector<int>>&, int, int, int, int, int);

virtual std::vector<Node> GetNeighbours(const std::vector<std::vector<int>>&, Node *, int, int);

int ReadfromXml(const char*);

virtual std::vector<Node> MakeShort(const std::vector<Node>&);

virtual std::vector<Node> MakeLong(const std::vector<Node>&);

Logger DoAstar(const std::vector<std::vector<int>>&, const char *, double, int, int, int, int);

};

class AStar : public Search {};

class JPSearch : public Search {
public:

    std::vector<Node> MakeShort(const std::vector<Node>&);

    std::vector<Node> MakeLong(const std::vector<Node>&);

    int ExploreDirection(const std::vector<std::vector<int>> &, int, int, int, int, int, int);

    int ExploreDiagonal(const std::vector<std::vector<int>> &, int, int, int, int, int, int);

    std::vector<Node> GetNeighbours(const std::vector<std::vector<int>> &, Node *, int, int);
};

class ThetaStar : public Search {
public:

    bool LineofSight(const std::vector<std::vector<int>>&, int, int, int, int);

    void AddtoPath(std::vector<Node>& path, int x1, int y1, int x2, int y2);

    std::vector<Node> MakeShort(const std::vector<Node>&);

    std::vector<Node> MakeLong(const std::vector<Node>&);

    void ResetParent(const std::vector<std::vector<int>>&, Node&, int, int);
};


