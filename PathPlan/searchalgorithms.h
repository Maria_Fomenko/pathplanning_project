#include <algorithm>
#include <iostream>
#include <limits>
#include <vector>

struct Node {
    int x;
    int y;
    double g_func;
    double f_func;
    Node *parent;
};

struct heap_element {
    double key;
    Node element;
};

bool operator < (const heap_element& first, const heap_element& second) {
    return (first.key < second.key);
}

class Heap {
private:
    std::vector<heap_element> heap;

int first_child(int current) {
    return 2 * current + 1;
}

int second_child(int current) {
    return 2 * current + 2;
}

int parent(int current) {
    return (current - 1) / 2;
}

public:
    void push(Node elem) {
        heap_element element;
        element.element = elem;
        element.key = elem.f_func;
        heap.push_back(element);
        int current = heap.size() - 1;
        while (current >= 1 && heap[current] < heap[parent(current)]) {
            std::swap(heap[current], heap[parent(current)]);
            current = parent(current);
        }
    };
    int Shift_down(int current) {
        int another;
        if ((second_child(current) < heap.size()) &&
            (heap[second_child(current)] < heap[first_child(current)])) {
            another = second_child(current);
        } else {
            another = first_child(current);
        }
        if (heap[another] < heap[current]) {
            std::swap(heap[another], heap[current]);
        }
        return another;
    }

    Node top() const {
        return heap[0].element;
    }

    void pop() {
        if (heap.size() != 0) {
            int current = 0;
            std::swap(heap[0], heap[heap.size() - 1]);
            heap.pop_back();
            while (first_child(current) < heap.size()) {
                current = Shift_down(current);
            }
        }
    }

    void decrease_key(double key, Node element) {
        int current = 0;
        while (current != heap.size() &&
               (element.x != heap[current].element.x || element.y != heap[current].element.y)) ++current;
        if (current != heap.size()) {
            heap[current].key = key;
            heap[current].element.g_func = element.g_func;
            heap[current].element.f_func = element.f_func;
        } else {
            heap_element elem;
            elem.element = element;
            elem.key = key;
            heap.push_back(elem);
            current = heap.size() - 1;
        }
        while (current >= 1 && heap[current] < heap[parent(current)]) {
            std::swap(heap[current], heap[parent(current)]);
            current = parent(current);
        }
    }

    bool empty() const {
        return heap.empty();
    }

    int size() const {
        return heap.size();
    }
};

std::vector<Node> GetNeighbours(const std::vector<std::vector<int>>& grid, int x, int y) {
    std::vector<Node> result;
    Node new_node;
    if (x != 0 && y != 0 && grid[y-1][x-1] == 0) result.push_back(new_node);
    return result;
}

void calculate_path(const std::vector<std::vector<int>>& grid, int startx, int starty, int finishx, int finishy) {
    Node start;
    start.x = startx;
    start.y = starty;
    Heap opened;
    std::vector<Node> closed;
    opened.push(start);
    while (!opened.empty()) {
        Node vertex = opened.top();
        opened.pop();
        std::vector<Node> neigbours = GetNeighbours(grid, vertex.x, vertex.y);
        /*for (int number = 0; number != graph[vertex].size(); ++number) {
            int length = roads[vertex][number].length;
            if (vertex.g_func + length < 0) {
                distance[to] = distance[vertex] + length;
                unchecked.decrease_key(to, distance[to]);
                parent[to] = vertex;
            }
        }*/
    }
}

