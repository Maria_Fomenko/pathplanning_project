#include <map>
#include <vector>
#include <structs.h>
#include <math.h>
#include <string.h>
#include <logger.h>
#include <searchfunc.h>
#include <search.h>

int JPSearch::ExploreDirection(const std::vector<std::vector<int>> &grid, int x, int y, int dx, int dy, int finx, int finy) {
    int x0 = x;
    int y0 = y;
    bool if_forced = 0;
    int adx, ady;
    if (dx == 0) {
        adx = 1;
        ady = 0;
    } else {
        adx = 0;
        ady = 1;
    }
    while (!if_forced && (x != finx || y != finy)) {
        x += dx;
        y += dy;
        if (!IfNeighbour(grid, x - dx, y - dy, x, y, 1)) return 0;
        if ((IfNeighbour(grid, x, y, x + dx + adx, y + dy + ady, 2) && grid[y + ady][x + adx]) ||
        (IfNeighbour(grid, x, y, x + dx - adx, y + dy - ady, 2) && grid[y - ady][x - adx])) {
            if_forced = 1;
        }
        if ((!cutcorners) &&
                ((IfNeighbour(grid, x, y, x + adx, y + ady, 1) && grid[y + ady - dy][x + adx - dx]) ||
                 (IfNeighbour(grid, x, y, x - adx, y - ady, 1) && grid[y - ady - dy][x - adx - dx]) ||
                 (IfNeighbour(grid, x, y, x + adx + dx, y + ady + dy, 2) && grid[y + ady - dy][x + adx - dx]) ||
                 (IfNeighbour(grid, x, y, x - adx + dx, y - ady + dy, 2) && grid[y - ady - dy][x - adx - dx]))) {
            if_forced = 1;
        }
    }
    return abs(x - x0 + y - y0);
}

int JPSearch::ExploreDiagonal(const std::vector<std::vector<int>> &grid, int x, int y, int dx, int dy, int finx, int finy) {
    int x0 = x;
    bool if_forced = 0;
    bool if_change = 0;
    while (!if_forced && (x != finx || y != finy) && !if_change) {
        x += dx;
        y += dy;
        if (!IfNeighbour(grid, x - dx, y - dy, x, y, 2)) return 0;
        if ((IfNeighbour(grid, x, y, x - dx, y + dy, 2) && grid[y][x - dx]) ||
        (IfNeighbour(grid, x, y, x + dx, y - dy, 2) && grid[y - dy][x])) {
            if_forced = 1;
        }
        if (ExploreDirection(grid, x, y, dx, 0, finx, finy) != 0 || ExploreDirection(grid, x, y, 0, dy, finx, finy) != 0) if_change = 1;
    }
    return abs(x - x0);
}

std::vector<Node> JPSearch::GetNeighbours(const std::vector<std::vector<int>> &grid, Node *cur, int finx, int finy) {
    std::vector<Node> res;
    int x = cur->x;
    int y = cur->y;
    int l, nx, ny;
    if (cur->g_func == 0) {
        for (int i = -1; i != 2; ++i) {
            for (int j = -1; j != 2; ++j) {
                if (abs(i) + abs(j) == 1) {
                    if ((l = ExploreDirection(grid, x, y, i, j, finx, finy)) > 0) {
                        nx = x + l * i;
                        ny = y + l * j;
                        res.push_back(makeNode(nx, ny, cur->g_func + linecost * l, dist(nx, ny, finx, finy), cur));
                    }
                } else if (abs(i) + abs(j) == 2) {
                    if ((l = ExploreDiagonal(grid, x, y, i, j, finx, finy)) > 0) {
                        nx = x + l * i;
                        ny = y + l * j;
                        res.push_back(makeNode(nx, ny, cur->g_func + diagonalcost * l, dist(nx, ny, finx, finy), cur));
                    }
                }
            }
        }
        return res;
    }
    Node *par = cur->parent;
    int dx = cur->x - par->x;
    int dy = cur->y - par->y;
    if (dx != 0) dx /= abs(dx);
    if (dy != 0) dy /= abs(dy);
    int sum = abs(dx) + abs(dy);
    if (sum == 1) {
        int adx, ady;
        if (dx == 0) {
            adx = 1;
            ady = 0;
        } else {
            adx = 0;
            ady = 1;
        }
        if ((l = ExploreDirection(grid, x, y, dx, dy, finx, finy)) > 0) {
            nx = x + l * dx;
            ny = y + l * dy;
            res.push_back(makeNode(nx, ny, cur->g_func + linecost * l, dist(nx, ny, finx, finy), cur));
        }
        if (IfNeighbour(grid, x, y, x + dx + adx, y + dy + ady, 2) && grid[y + ady][x + adx]) {
            if ((l = ExploreDiagonal(grid, x, y, dx + adx, dy + ady, finx, finy)) > 0) {
                nx = x + l * (dx + adx);
                ny = y + l * (dy + ady);
                res.push_back(makeNode(nx, ny, cur->g_func + diagonalcost * l, dist(nx, ny, finx, finy), cur));
            }
        }
        if (IfNeighbour(grid, x, y, x + dx - adx, y + dy - ady, 2) && grid[y - ady][x - adx]) {
            if ((l = ExploreDiagonal(grid, x, y, dx - adx, dy - ady, finx, finy)) > 0) {
                nx = x + l * (dx - adx);
                ny = y + l * (dy - ady);
                res.push_back(makeNode(nx, ny, cur->g_func + diagonalcost * l, dist(nx, ny, finx, finy), cur));
            }
        }
        if (!cutcorners) {
            if (IfNeighbour(grid, x, y, x + adx, y + ady, 1) && grid[y + ady - dy][x + adx - dx]) {
                if ((l = ExploreDirection(grid, x, y, adx, ady, finx, finy)) > 0) {
                    nx = x + l * adx;
                    ny = y + l * ady;
                    res.push_back(makeNode(nx, ny, cur->g_func + linecost * l, dist(nx, ny, finx, finy), cur));
                }
            }
            if (IfNeighbour(grid, x, y, x + adx + dx, y + ady + dy, 2) && grid[y + ady - dy][x + adx - dx]) {
                if ((l = ExploreDiagonal(grid, x, y, dx + adx, dy + ady, finx, finy)) > 0) {
                    nx = x + l * (dx + adx);
                    ny = y + l * (dy + ady);
                    res.push_back(makeNode(nx, ny, cur->g_func + diagonalcost * l, dist(nx, ny, finx, finy), cur));
                }
            }
            if (IfNeighbour(grid, x, y, x - adx, y - ady, 1) && grid[y - ady - dy][x - adx - dx]) {
                if ((l = ExploreDirection(grid, x, y, -adx, -ady, finx, finy)) > 0) {
                    nx = x - l * adx;
                    ny = y - l * ady;
                    res.push_back(makeNode(nx, ny, cur->g_func + linecost * l, dist(nx, ny, finx, finy), cur));
                }
            }
            if (IfNeighbour(grid, x, y, x - adx + dx, y - ady + dy, 2) && grid[y - ady - dy][x - adx - dx]) {
                if ((l = ExploreDiagonal(grid, x, y, dx - adx, dy - ady, finx, finy)) > 0) {
                    nx = x + l * (dx - adx);
                    ny = y + l * (dy - ady);
                    res.push_back(makeNode(nx, ny, cur->g_func + diagonalcost * l, dist(nx, ny, finx, finy), cur));
                }
            }
        }
        return res;
    }
    if ((l = ExploreDiagonal(grid, cur->x, cur->y, dx, dy, finx, finy)) > 0) {
        nx = x + l * dx;
        ny = y + l * dy;
        res.push_back(makeNode(nx, ny, cur->g_func + diagonalcost * l, dist(nx, ny, finx, finy), cur));
    }
    if ((l = ExploreDirection(grid, x, y, dx, 0, finx, finy)) > 0) {
        nx = x + l * dx;
        ny = y;
        res.push_back(makeNode(nx, ny, cur->g_func + linecost * l, dist(nx, ny, finx, finy), cur));
    }
    if ((l = ExploreDirection(grid, x, y, 0, dy, finx, finy)) > 0) {
        nx = x;
        ny = y + l * dy;
        res.push_back(makeNode(nx, ny, cur->g_func + linecost * l, dist(nx, ny, finx, finy), cur));
    }
    if (IfNeighbour(grid, x, y, x + dx, y - dy, 2) && grid[y - dy][x]) {
        if ((l = ExploreDiagonal(grid, x, y, dx, -dy, finx, finy)) > 0) {
            nx = x + l * dx;
            ny = y - l * dy;
            res.push_back(makeNode(nx, ny, cur->g_func + diagonalcost * l, dist(nx, ny, finx, finy), cur));
        }
    }
    if (IfNeighbour(grid, x, y, x - dx, y + dy, 2) && grid[y][x - dx]) {
        if ((l = ExploreDiagonal(grid, x, y, -dx, dy, finx, finy)) > 0) {
            nx = x - l * dx;
            ny = y + l * dy;
            res.push_back(makeNode(nx, ny, cur->g_func + diagonalcost * l, dist(nx, ny, finx, finy), cur));
        }
    }
    return res;
}

std::vector<Node> JPSearch::MakeShort(const std::vector<Node>& path) {
    return path;
}

std::vector<Node> JPSearch::MakeLong(const std::vector<Node>& path) {
    std::vector<Node> result;
    for (size_t i = 1; i != path.size(); ++i) {
        int dx = path[i].x - path[i - 1].x;
        if (dx != 0) dx = dx / abs(dx);
        int dy = path[i].y - path[i - 1].y;
        if (dy != 0) dy = dy / abs(dy);
        int x = path[i - 1].x;
        int y = path[i - 1].y;
        while (x != path[i].x || y != path[i].y) {
            result.push_back(makeNode(x, y, 0, 0, NULL));
            x += dx;
            y += dy;
        }
    }
    result.push_back(path[path.size() - 1]);
    return result;
}
