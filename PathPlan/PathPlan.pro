QT += core
QT -= gui

CONFIG += c++11

TARGET = PathPlan
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

SOURCES += \
    tinystr.cpp \
    tinyxml.cpp \
    tinyxmlerror.cpp \
    tinyxmlparser.cpp \
    main.cpp \
    search.cpp \
    inputfunc.cpp \
    map.cpp \
    logger.cpp \
    searchfunc.cpp \
    jps.cpp \
    theta.cpp

DISTFILES += \
    PathPlan.pro.user \
    utf8test.xml \
    utf8testverify.xml \
    utf8test.gif \
    changes.txt \
    tinyxml.sln \
    tinyxml_lib.vcxproj \
    tinyxmlSTL.vcxproj \
    tinyXmlTest.vcxproj \
    tinyXmlTestSTL.vcxproj \
    Makefile \
    3664933.xml

HEADERS += \
    tinystr.h \
    tinyxml.h \
    transform.h \
    structs.h \
    searchalgorithms.h \
    dijkstra.h \
    defines.h \
    map.h \
    search.h \
    inputfunc.h \
    logger.h \
    includes.h \
    outputfunc.h \
    searchfunc.h

QMAKE_LFLAGS_RELEASE += -static -static-libgcc
