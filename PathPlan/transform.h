#include <vector>
#include <string>
#include <structs.h>

int ToNumber(int i, int j, int width) {
    return width * i + j;
}

int ToYCoord(int number, int width) {
    return number / width;
}

int ToXCoord(int number, int width) {
    return number % width;
}

void AddHoriz(const std::vector<std::vector<int>>& grid, std::vector<std::vector<arrow>>& graph,
              int i1, int j1, int i2, int j2, int width, double linecost) {
    if (grid[i2][j2] == 1) {
        arrow new_arrow;
        new_arrow.dest = ToNumber(i2, j2, width);
        new_arrow.length = linecost;
        graph[ToNumber(i1, j1, width)].push_back(new_arrow);
    }
}

void AddSqDiag(const std::vector<std::vector<int>>& grid, std::vector<std::vector<arrow>>& graph,
              int i1, int j1, int i2, int j2, int width, double diagcost) {
    if (grid[i2][j2] == 1) {
        arrow new_arrow;
        new_arrow.dest = ToNumber(i2, j2, width);
        new_arrow.length = diagcost;
        graph[ToNumber(i1, j1, width)].push_back(new_arrow);
    }
}


void AddDiag(const std::vector<std::vector<int>>& grid, std::vector<std::vector<arrow>>& graph,
              int i1, int j1, int i2, int j2, int width, double diagcost) {
    if (grid[i2][j2] == 1 && grid[i1][j2] == 1 && grid[i2][j1] == 1) {
        arrow new_arrow;
        new_arrow.dest = ToNumber(i2, j2, width);
        new_arrow.length = diagcost;
        graph[ToNumber(i1, j1, width)].push_back(new_arrow);
    }
}

void Add(const std::vector<std::vector<int>>& grid, std::vector<std::vector<arrow>>& graph,
         int allowd, int allowsq, size_t i, size_t j, int width, double linecost, double diagcost) {
    if (i != 0) AddHoriz(grid, graph, i, j, i-1, j, width, linecost);
    if (i != grid.size() - 1) AddHoriz(grid, graph,i, j, i+1, j, width, linecost);
    if (j != 0) AddHoriz(grid, graph, i, j, i, j-1, width, linecost);
    if (j != grid[i].size() - 1) AddHoriz(grid, graph, i, j, i, j+1, width, linecost);
    if (allowd && allowsq) {
        if (i != 0 && j != 0) AddSqDiag(grid, graph, i, j, i-1, j-1, width, diagcost);
        if (i != 0 && j != grid[i].size() - 1) AddSqDiag(grid, graph, i, j, i-1, j+1, width, diagcost);
        if (i != grid.size() - 1 && j != 0) AddSqDiag(grid, graph, i, j, i+1, j-1, width, diagcost);
        if (i != grid.size() - 1 && j != grid[i].size()) AddSqDiag(grid, graph, i, j, i+1, j+1, width, diagcost);
    } else if (allowd) {
        if (i != 0 && j != 0) AddSqDiag(grid, graph, i, j, i-1, j-1, width, diagcost);
        if (i != 0 && j != grid[i].size() - 1) AddSqDiag(grid, graph, i, j, i-1, j+1, width, diagcost);
        if (i != grid.size() - 1 && j != 0) AddSqDiag(grid, graph, i, j, i+1, j-1, width, diagcost);
        if (i != grid.size() - 1 && j != grid[i].size()) AddSqDiag(grid, graph, i, j, i+1, j+1, width, diagcost);
    }
}

std::vector<std::vector<arrow>> TransformInput(const std::vector<std::vector<int>>& grid, int allowd, int allowsq,
                                               int width, double linecost, double diagcost) {
    std::vector<std::vector<arrow>> graph;
    for (size_t i = 0; i != grid.size(); ++i) {
        for (size_t j = 0; j != grid[i].size(); ++j) {
            if (grid[i][j] == 1) Add(grid, graph, allowd, allowsq, i, j, width, linecost, diagcost);
        }
    }
    return graph;
}
